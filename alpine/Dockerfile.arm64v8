FROM alpine as qemu

RUN if [ -n "aarch64" ]; then \
                wget -O /qemu-aarch64-static https://github.com/multiarch/qemu-user-static/releases/download/v4.2.0-2/qemu-aarch64-static; \
        else \
                echo '#!/bin/sh\n\ntrue' > /qemu-aarch64-static; \
        fi; \
        chmod a+x /qemu-aarch64-static

FROM arm64v8/alpine:3.10 as builder

COPY --from=qemu /qemu-aarch64-static /usr/bin/

RUN apk update; apk add \
	build-base \
	findutils \
	git \
	curl-dev \
	openssl-dev \
	jansson-dev \
	libnl3-dev \
	gettext \
	linux-headers

WORKDIR /work

COPY Makefile .
ADD src src
ADD files files
ADD rules rules
ADD libs libs
ADD extras extras

RUN make
RUN mkdir /dist/; tar -C /dist/ -xvzf /work/out/linux_generic/linux_generic-*.tgz

FROM arm64v8/alpine:3.10

COPY --from=qemu /qemu-aarch64-static /usr/bin/

RUN apk update; apk add \
        curl-dev \
        libssl1.1 \
	jansson \
        libnl3; \
	mkdir -p /var/opt/unum/

COPY --from=builder /dist/lib /lib
COPY --from=builder /dist/sbin /sbin
COPY --from=builder /dist/bin /bin
COPY --from=builder /dist/dist/etc /etc

CMD [ "/bin/unum" ]

