#!/bin/sh

set -e

ARCHS=${ARCHS:-amd64:x86_64 arm32v6:arm arm64v8:aarch64}

cat > ../.gitlab-ci.yml << EOF
include:
  local: build-dockerfile.yml

EOF


for a in $ARCHS; do
	qemu_arch=`echo $a | sed -e 's/.*://'`
	arch=`echo $a | sed -e 's/:.*//'`
	cat Dockerfile.template | ARCH=$arch QEMU_ARCH=$qemu_arch envsubst > Dockerfile.$arch
        cat >> ../.gitlab-ci.yml << EOF2

build-$arch:
  extends: .build-dockerfile
  variables:
    ARCH: $arch
    QEMU_ARCH: $qemu_arch
    DOCKERFILE_PATH: alpine/
EOF2

done

